<html>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link rel="stylesheet" href="/modules/personnalStats.css">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/paho-mqtt/1.0.1/mqttws31.min.js" type="text/javascript"></script>
<script src="js/personnalStats.js"></script>
</head>
<body>
    <h1>PIPBOY</h1>

    <div class="row">
		<div class="col-md-4"><img src="img/vault_boy.png" class="img-responsive" alt="vault_boy"></div>
		<div class="col-md-8"> 
			<div class="row"><div class="col-md-4"><p>Fréquence cardiaque</p></div>
							<div class="col-md-4">120</div>			
			</div>
			<div class="row"><div class="col-md-4">Température</div>
							<div class="col-md-4">37</div>			
			</div>
			<div class="row"><div class="col-md-4">Niveau d'hydratation</div>
							<div class="col-md-4">78%</div>			
			</div>
			<div class="row"><div class="col-md-4">Tension</div>
							<div class="col-md-4">7.5/13</div>			
			</div>
			<div class="row"><div class="col-md-4">Fréquence respiratoire</div>
							<div class="col-md-4">20</div>			
			</div>
        </div>


</body>
</html>
